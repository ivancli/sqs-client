import apiClient from '../services/apiClient'
import Message from './Message'

export default class Form {
  /**
   * Create a new Form instance.
   *
   * @param {object} data
   */
  constructor (data) {
    this.originalData = data

    for (const field in data) {
      this[field] = data[field]
    }

    this.message = new Message()
    this.submitting = false
    this.responded = false
  }

  /**
   * Fetch all relevant data for the form.
   */
  data () {
    const data = {}

    for (const property in this.originalData) {
      data[property] = this[property]
    }

    return data
  }

  /**
   * Reset the form fields.
   */
  reset () {
    for (const field in this.originalData) {
      this[field] = ''
    }

    this.message.clear()
  }

  /**
   * Send a POST request to the given URL.
   * .
   * @param {string} url
   * @param {object|undefined} config
   */
  post (url, config = undefined) {
    return this.submit('post', url, config)
  }

  /**
   * Send a PUT request to the given URL.
   * .
   * @param {string} url
   * @param {object} config
   */
  put (url, config) {
    return this.submit('put', url, config)
  }

  /**
   * Send a PATCH request to the given URL.
   * .
   * @param {string} url
   * @param {object} config
   */
  patch (url, config) {
    return this.submit('patch', url, config)
  }

  /**
   * Send a DELETE request to the given URL.
   * .
   * @param {string} url
   * @param {object} config
   */
  delete (url, config) {
    return this.submit('delete', url, config)
  }

  /**
   * Submit the form.
   *
   * @param {string} requestType
   * @param {string} url
   * @param {object} config
   */
  submit (requestType, url, config = {}) {
    this.message.clear()
    this.submitting = true
    this.responded = false
    return new Promise((resolve, reject) => {
      // eslint-disable-next-line no-undef
      apiClient[requestType.toLowerCase()](url, this.data(), config)
        .then((response) => {
          this.submitting = false
          this.responded = true
          this.onSuccess(response.data)
          resolve(response.data, response.status)
        })
        .catch((error) => {
          this.submitting = false
          this.responded = true
          if (error.response.status === 302) {
            this.onSuccess(error.response.data)
            resolve(error.response.data, error.response.status)
          } else if (error.response.status === 419) {
            this.onFail({
              message: 'Your session has expired, please refresh the page and try again.'
            }, error.response.status)
            reject(error.response.data, error.response.status)
          } else {
            this.onFail(error.response.data, error.response.status)
            reject(error.response.data, error.response.status)
          }
        })
    })
  }

  /**
   * Handle a successful form submission.
   *
   * @param {object} data
   */
  onSuccess (data) {
  }

  /**
   * Handle a failed form submission.
   *
   * @param {object} errors
   * @param {number} status
   */
  onFail (errors, status) {
    this.message.record(errors, status)
  }
}

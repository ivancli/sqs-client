export default class Message {
  /**
   * Create a new Errors instance.
   */
  constructor () {
    this.code = null
    this.errors = {}
    this.message = ''
    this.courtesy_error = 'We are experiencing technical issues with the website, please try again later.'
  }

  /**
   * Determine if an errors exists for the given field.
   *
   * @param {string} field
   */
  has (field) {
    return typeof this.errors[field] !== 'undefined'
  }

  /**
   * Determine if we have any errors.
   */
  any () {
    return Object.keys(this.errors).length > 0
  }

  /**
   * Retrieve the error message for a field.
   *
   * @param {string} field
   */
  get (field) {
    if (this.errors[field]) {
      return this.errors[field][0]
    }
  }

  title () {
    return this.message
  }

  fieldErrors () {
    return this.errors
  }

  /**
   * Record the new errors.
   *
   * @param {object} errors
   * @param {number} code
   */
  record (errors, code) {
    this.errors = errors.errors
    this.message = errors.message
    if (typeof code !== 'undefined') {
      this.code = code
    }
  }

  /**
   * Clear one or all error fields.
   *
   * @param {string|null|undefined} field
   */
  clear (field) {
    if (field) {
      delete this.errors[field]

      return
    }

    this.errors = {}
    this.code = null
  }
}

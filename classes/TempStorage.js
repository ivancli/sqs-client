export default class TempStorage {
  /**
   * prefix of all keys
   * @returns {string}
   */
  static get prefix () {
    return 'rp_'
  }

  /**
   * check local storage availability
   * @returns {boolean}
   */
  static isLocalStorageAvailable () {
    const test = 'test'
    try {
      localStorage.setItem(test, test)
      localStorage.removeItem(test)
      return true
    } catch (e) {
      return false
    }
  }

  /**
   * store an item
   * @param key
   * @param value
   */
  static set (key, value) {
    console.log('set')
    if (TempStorage.isLocalStorageAvailable() === true) {
      localStorage.setItem(TempStorage.prefix + key, value)
    } else {
      TempStorage.setCookie(TempStorage.prefix + key, value)
    }
  }

  /**
   * get an item
   * @param key
   * @param cookie {boolean}
   * @returns {string | null}
   */
  static get (key, cookie) {
    console.log('get')
    if (TempStorage.isLocalStorageAvailable() === false || cookie === true) {
      return TempStorage.getCookie(TempStorage.prefix + key)
    } else {
      return localStorage.getItem(TempStorage.prefix + key)
    }
  }

  /**
   * delete an item
   * @param key
   * @param cookie {boolean}
   */
  static del (key, cookie) {
    if (TempStorage.isLocalStorageAvailable() === false || cookie === true) {
      return TempStorage.delCookie(TempStorage.prefix + key)
    } else {
      return localStorage.removeItem(TempStorage.prefix + key)
    }
  }

  /**
   * get item from cookie by name
   * @param cookieName
   * @returns {string}
   */
  static getCookie (cookieName) {
    let cookieValue = document.cookie
    let cookieStart = cookieValue.indexOf(' ' + cookieName + '=')
    if (cookieStart === -1) {
      cookieStart = cookieValue.indexOf(cookieName + '=')
    }

    if (cookieStart === -1) {
      cookieValue = null
    } else {
      cookieStart = cookieValue.indexOf('=', cookieStart) + 1
      let cookieend = cookieValue.indexOf(';', cookieStart)
      if (cookieend === -1) {
        cookieend = cookieValue.length
      }

      cookieValue = unescape(cookieValue.substring(cookieStart, cookieend))
    }

    return cookieValue
  }

  /**
   * set item in cookie
   * @param cookieName
   * @param value
   * @param exdays
   */
  static setCookie (cookieName, value, exdays) {
    const exdate = new Date()
    exdate.setDate(exdate.getDate() + exdays)
    const cookieValue = escape(value) + ((exdays == null) ? '' : '; expires=' + exdate.toUTCString())
    document.cookie = cookieName + '=' + cookieValue
  }

  /**
   * delete item from cookie
   * @param name
   */
  static delCookie (name) {
    TempStorage.setCookie(name, '', -1)
  }
}

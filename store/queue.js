import apiClient from '~/services/apiClient'
import mutation from '~/constants/mutations'

export const state = () => ({
  list: []
})

export const mutations = {
  [mutation.QUEUE.LIST.SET] (state, data) {
    state.list = data
  }
}

export const actions = {
  load (context) {
    apiClient.get('/api/queue').then((response) => {
      context.commit(mutation.QUEUE.LIST.SET, response.data)
    })
  }
}

export const state = () => ({
  list: [
    {
      name: 'index',
      title: 'Home',
      path: '/',
      child: [
        {
          name: 'queue',
          title: 'Queue',
          path: '/queue',
          child: [
            {
              name: 'queue-name',
              title: ':name',
              path: '/queue/:name'
            }
          ]
        }
      ]
    }
  ]
})

export const getters = {
  current (state) {
    return (name) => {
      const flatItems = []
      const search = (item) => {
        if (item.name === name) {
          return true
        }
        if (item.child) {
          const matchedChild = item.child.filter((child) => {
            return search(child)
          })
          if (matchedChild.length === 1) {
            const finalItem = {
              text: matchedChild[0].title,
              href: matchedChild[0].path,
              disabled: false
            }
            if (typeof matchedChild[0].child === 'undefined') {
              finalItem.disabled = true
            }
            flatItems.unshift(finalItem)

            return matchedChild[0]
          }
        }
      }
      const matchedChild = state.list.filter((item) => {
        return search(item)
      })

      if (matchedChild.length === 1) {
        const finalItem = {
          text: matchedChild[0].title,
          href: matchedChild[0].path,
          disabled: false
        }
        if (typeof matchedChild[0].child === 'undefined') {
          finalItem.disabled = true
        }
        flatItems.unshift(finalItem)
        return flatItems
      }

      return []
    }
  }
}

const AWS = require('aws-sdk')
const credentials = new AWS.Credentials('x', 'x')
AWS.config.credentials = credentials
const endpoint = new AWS.Endpoint('http://elasticmq:9324')
const region = 'ap-southeast-2'
const sqs = new AWS.SQS({ endpoint, region })

class Queue {
  static list () {
    return new Promise((resolve, reject) => {
      sqs.listQueues({}, (error, data) => {
        if (error) {
          reject(error)
        } else {
          const queues = []
          if (data.QueueUrls) {
            data.QueueUrls.forEach((queueUrl) => {
              const url = new URL(queueUrl)
              const name = url.pathname.replace('/queue/', '')
              queues.push({
                name,
                link: queueUrl
              })
            })
          }
          resolve(queues)
        }
      })
    })
  }

  static get (url) {
    return new Promise((resolve, reject) => {
      sqs.getQueueAttributes({
        QueueUrl: url,
        AttributeNames: ['All']
      }, (error, data) => {
        if (error) {
          reject(error)
        } else {
          resolve(data.Attributes)
        }
      })
    })
  }

  static create (params) {
    return new Promise((resolve, reject) => {
      sqs.createQueue(params, (error, data) => {
        if (error) {
          reject(error)
        } else {
          resolve(data)
        }
      })
    })
  }

  static delete (url) {
    return new Promise((resolve, reject) => {
      sqs.deleteQueue({ QueueUrl: url }, (error, data) => {
        if (error) {
          reject(error)
        } else {
          resolve(data)
        }
      })
    })
  }
}

module.exports = Queue

const express = require('express')
const Queue = require('../../models/queue')

const router = express.Router()

// middleware that is specific to this router
router.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now())
  next()
})

// list all queues
router.get('/', (req, res) => {
  Queue.list().then((queues) => {
    res.send(queues)
  }).catch((error) => {
    res.status(400)
    res.send(error)
  })
})

router.post('/', (req, res) => {
  const params = req.body
  Object.keys(params.Attributes).map((key, index) => {
    params.Attributes[key] = params.Attributes[key].toString()
  })
  console.log(params)
  Queue.create(params).then((result) => {
    res.send(result)
  })
})

// get queue details by queue name
router.get('/:name', (req, res) => {
  const queueName = req.params.name
  Queue.list().then((queues) => {
    const matchedQueue = queues.filter((queue) => {
      return queue.name === queueName
    })[0]
    Queue.get(matchedQueue.link).then((queue) => {
      res.send(queue)
    }).catch((error) => {
      res.status(400)
      res.send(error)
    })
  }).catch((error) => {
    res.status(400)
    res.send(error)
  })
})

// get queue details by queue name
router.delete('/:name', (req, res) => {
  const queueName = req.params.name
  Queue.list().then((queues) => {
    const matchedQueue = queues.filter((queue) => {
      return queue.name === queueName
    })[0]
    console.log('matchedQueue', matchedQueue)
    if (matchedQueue) {
      Queue.delete(matchedQueue.link).then((result) => {
        res.send(result)
      }).catch((error) => {
        res.status(400)
        res.send(error)
      })
    }
  }).catch((error) => {
    res.status(400)
    res.send(error)
  })
})

module.exports = router

const express = require('express')
const router = express.Router()
const queue = require('./queue')

// middleware that is specific to this router
router.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now())
  next()
})

router.use('/queue', queue)

module.exports = router

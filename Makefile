clean :
	docker-compose down -v

start :
	docker-compose up -d --build --force-recreate --remove-orphans

restart :
	make clean
	make start

log-node :
	docker logs sc-node -f

log-nginx :
	docker logs sc-nginx -f
